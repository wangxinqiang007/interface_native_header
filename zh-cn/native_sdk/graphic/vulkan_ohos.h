#ifndef VULKAN_OHOS_H_
#define VULKAN_OHOS_H_ 1

/*
** Copyright 2015-2022 The Khronos Group Inc.
**
** SPDX-License-Identifier: Apache-2.0
*/

/*
** This header is generated from the Khronos Vulkan XML API Registry.
**
*/


/**
 * @addtogroup Vulkan
 * @{
 *
 * @brief 提供OpenHarmony平台扩展的Vulkan能力，扩展了使用OHNativeWindow创建Vulkan Surface的能力，以及获取OH_NativeBuffer和OH_NativeBuffer属性的能力。
 *
 * @syscap SystemCapability.Graphic.Vulkan
 * @since 10
 * @version 1.0
 */

/**
 * @file vulkan_ohos.h
 *
 * @brief 定义了OpenHarmony平台扩展的Vulkan接口。引用文件：<vulkan/vulkan.h>。
 *
 * @library libvulkan.so
 * @since 10
 * @version 1.0
 */

#ifdef __cplusplus
extern "C" {
#endif


/**
 * @brief OpenHarmony平台Surface扩展宏定义。
 * @since 10
 * @version 1.0
 */
#define VK_OHOS_surface 1

/**
 * @brief OH本地窗口。
 * @since 10
 * @version 1.0
 */
typedef struct NativeWindow OHNativeWindow;

/**
 * @brief OpenHarmony平台Surface扩展版本号。
 * @since 10
 * @version 1.0
 */
#define VK_OHOS_SURFACE_SPEC_VERSION      1

/**
 * @brief OpenHarmony平台Surface扩展名。
 * @since 10
 * @version 1.0
 */
#define VK_OHOS_SURFACE_EXTENSION_NAME    "VK_OHOS_surface"

/**
 * @brief 用于Vulkan Surface创建时使用到的VkFlags类型位掩码，预留的标志类型。
 * @since 10
 * @version 1.0
 */
typedef VkFlags VkSurfaceCreateFlagsOHOS;

/**
 * @brief 包含创建Vulkan Surface时必要的参数。
 * @since 10
 * @version 1.0
 */
typedef struct VkSurfaceCreateInfoOHOS {
    /**
     * 结构体类型。
     */
    VkStructureType             sType;
    /**
     * 下一级结构体指针。
     */
    const void*                 pNext;
    /**
     * 预留的标志类型参数。
     */
    VkSurfaceCreateFlagsOHOS    flags;
    /**
     * OHNativeWindow指针。
     */
    OHNativeWindow*             window;
} VkSurfaceCreateInfoOHOS;

/**
 * @brief 创建Vulkan Surface的函数指针定义。
 *
 * @syscap SystemCapability.Graphic.Vulkan
 * @param instance Vulkan实例。
 * @param pCreateInfo 一个VkSurfaceCreateInfoOHOS结构体的指针，包含创建Vulkan Surface时必要的参数。
 * @param pAllocator 用户自定义内存分配的回调函数，如果不需要可以传入NULL，接口会使用默认的内存分配函数。
 * @param pSurface 出参，用于接收创建的Vulkan Surface，类型为VkSurfaceKHR。
 * @return 返回一个VkResult类型的错误码，返回值为VK_SUCCESS表示执行成功。
 * @since 10
 * @version 1.0
 */
typedef VkResult (VKAPI_PTR *PFN_vkCreateSurfaceOHOS)(
    VkInstance                     instance,
    const VkSurfaceCreateInfoOHOS* pCreateInfo,
    const VkAllocationCallbacks*   pAllocator,
    VkSurfaceKHR*                  pSurface
);

#ifndef VK_NO_PROTOTYPES

/**
 * @brief 创建Vulkan Surface。
 *
 * @syscap SystemCapability.Graphic.Vulkan
 * @param instance Vulkan实例。
 * @param pCreateInfo 一个VkSurfaceCreateInfoOHOS结构体的指针，包含创建Vulkan Surface时必要的参数。
 * @param pAllocator 用户自定义内存分配的回调函数，如果不需要可以传入NULL，接口会使用默认的内存分配函数。
 * @param pSurface 出参，用于接收创建的Vulkan Surface，类型为VkSurfaceKHR。
 * @return 返回一个VkResult类型的错误码，返回值为VK_SUCCESS表示执行成功。
 * @since 10
 * @version 1.0
 */
VKAPI_ATTR VkResult VKAPI_CALL vkCreateSurfaceOHOS(
    VkInstance                                  instance,
    const VkSurfaceCreateInfoOHOS*              pCreateInfo,
    const VkAllocationCallbacks*                pAllocator,
    VkSurfaceKHR*                               pSurface);
#endif


/**
 * @brief OpenHarmony平台external_memory扩展宏定义。
 * @since 10
 * @version 1.0
 */
#define VK_OHOS_external_memory 1

/**
 * @brief OH_NativeBuffer结构体声明。
 * @since 10
 * @version 1.0
 */
struct OH_NativeBuffer;

/**
 * @brief OpenHarmony平台external_memory扩展版本号。
 * @since 10
 * @version 1.0
 */
#define VK_OHOS_EXTERNAL_MEMORY_SPEC_VERSION 1

/**
 * @brief OpenHarmony平台external_memory扩展名。
 * @since 10
 * @version 1.0
 */
#define VK_OHOS_EXTERNAL_MEMORY_EXTENSION_NAME "VK_OHOS_external_memory"

/**
 * @brief 提供OpenHarmony NativeBuffer用途的说明。
 * @since 10
 * @version 1.0
 */
typedef struct VkNativeBufferUsageOHOS {
    /**
     * 结构体类型。
     */
    VkStructureType    sType;
    /**
     * 下一级结构体指针。
     */
    void*              pNext;
    /**
     * NativeBuffer的用途说明。
     */
    uint64_t           OHOSNativeBufferUsage;
} VkNativeBufferUsageOHOS;

/**
 * @brief 包含了NaitveBuffer的属性。
 * @since 10
 * @version 1.0
 */
typedef struct VkNativeBufferPropertiesOHOS {
    /**
     * 结构体类型。
     */
    VkStructureType    sType;
    /**
     * 下一级结构体指针。
     */
    void*              pNext;
    /**
     * 占用的内存大小。
     */
    VkDeviceSize       allocationSize;
    /**
     * 内存类型。
     */
    uint32_t           memoryTypeBits;
} VkNativeBufferPropertiesOHOS;

/**
 * @brief 包含了NaitveBuffer的一些格式属性。
 * @since 10
 * @version 1.0
 */
typedef struct VkNativeBufferFormatPropertiesOHOS {
    /**
     * 结构体类型。
     */
    VkStructureType                  sType;
    /**
     * 下一级结构体指针。
     */
    void*                            pNext;
    /**
     * 格式说明。
     */
    VkFormat                         format;
    /**
     * 外部定义的格式标识符。
     */
    uint64_t                         externalFormat;
    /**
     * 描述了与externalFormat对应的能力。
     */
    VkFormatFeatureFlags             formatFeatures;
    /**
     * 表示一组VkComponentSwizzle。
     */
    VkComponentMapping               samplerYcbcrConversionComponents;
    /**
     * 色彩模型。
     */
    VkSamplerYcbcrModelConversion    suggestedYcbcrModel;
    /**
     * 色彩数值范围。
     */
    VkSamplerYcbcrRange              suggestedYcbcrRange;
    /**
     * X色度偏移。
     */
    VkChromaLocation                 suggestedXChromaOffset;
    /**
     * Y色度偏移。
     */
    VkChromaLocation                 suggestedYChromaOffset;
} VkNativeBufferFormatPropertiesOHOS;

/**
 * @brief 包含了OH_NativeBuffer结构体的指针。
 * @since 10
 * @version 1.0
 */
typedef struct VkImportNativeBufferInfoOHOS {
    /**
     * 结构体类型。
     */
    VkStructureType            sType;
    /**
     * 下一级结构体指针。
     */
    const void*                pNext;
    /**
     * OH_NativeBuffer结构体的指针。
     */
    struct OH_NativeBuffer*    buffer;
} VkImportNativeBufferInfoOHOS;

/**
 * @brief 用于从Vulkan内存中获取OH_NativeBuffer。
 * @since 10
 * @version 1.0
 */
typedef struct VkMemoryGetNativeBufferInfoOHOS {
    /**
     * 结构体类型。
     */
    VkStructureType    sType;
    /**
     * 下一级结构体指针。
     */
    const void*        pNext;
    /**
     * VkDeviceMemory对象。
     */
    VkDeviceMemory     memory;
} VkMemoryGetNativeBufferInfoOHOS;

/**
 * @brief 表示外部定义的格式标识符。
 * @since 10
 * @version 1.0
 */
typedef struct VkExternalFormatOHOS {
    /**
     * 结构体类型。
     */
    VkStructureType    sType;
    /**
     * 下一级结构体指针。
     */
    void*              pNext;
    /**
     * 外部定义的格式标识符。
     */
    uint64_t           externalFormat;
} VkExternalFormatOHOS;

/**
 * @brief 获取OH_NativeBuffer属性的函数指针定义。
 *
 * @syscap SystemCapability.Graphic.Vulkan
 * @param device VkDevice对象。
 * @param buffer OH_NativeBuffer结构体指针。
 * @param pProperties 用于接收OH_NativeBuffer属性的结构体。
 * @return 返回一个VkResult类型的错误码，返回值为VK_SUCCESS表示执行成功。
 * @since 10
 * @version 1.0
 */
typedef VkResult (VKAPI_PTR *PFN_vkGetNativeBufferPropertiesOHOS)(
    VkDevice                      device,
    const struct OH_NativeBuffer* buffer,
    VkNativeBufferPropertiesOHOS* pProperties
);

/**
 * @brief 获取OH_NativeBuffer的函数指针定义。
 *
 * @syscap SystemCapability.Graphic.Vulkan
 * @param device VkDevice对象。
 * @param pInfo VkMemoryGetNativeBufferInfoOHOS结构体对象。
 * @param pBuffer 用于接收获取到的OH_NativeBuffer。
 * @return 返回一个VkResult类型的错误码，返回值为VK_SUCCESS表示执行成功。
 * @since 10
 * @version 1.0
 */
typedef VkResult (VKAPI_PTR *PFN_vkGetMemoryNativeBufferOHOS)(
    VkDevice                               device,
    const VkMemoryGetNativeBufferInfoOHOS* pInfo,
    struct OH_NativeBuffer**               pBuffer
);

#ifndef VK_NO_PROTOTYPES

/**
 * @brief 获取OH_NativeBuffer属性。
 *
 * @syscap SystemCapability.Graphic.Vulkan
 * @param device VkDevice对象。
 * @param buffer OH_NativeBuffer结构体指针。
 * @param pProperties 用于接收OH_NativeBuffer属性的结构体。
 * @return 返回一个VkResult类型的错误码，返回值为VK_SUCCESS表示执行成功。
 * @since 10
 * @version 1.0
 */
VKAPI_ATTR VkResult VKAPI_CALL vkGetNativeBufferPropertiesOHOS(
    VkDevice                                    device,
    const struct OH_NativeBuffer*               buffer,
    VkNativeBufferPropertiesOHOS*               pProperties);

/**
 * @brief 获取OH_NativeBuffer。
 *
 * @syscap SystemCapability.Graphic.Vulkan
 * @param device VkDevice对象。
 * @param pInfo VkMemoryGetNativeBufferInfoOHOS结构体对象。
 * @param pBuffer 用于接收获取到的OH_NativeBuffer。
 * @return 返回一个VkResult类型的错误码，返回值为VK_SUCCESS表示执行成功。
 * @since 10
 * @version 1.0
 */
VKAPI_ATTR VkResult VKAPI_CALL vkGetMemoryNativeBufferOHOS(
    VkDevice                                    device,
    const VkMemoryGetNativeBufferInfoOHOS*      pInfo,
    struct OH_NativeBuffer**                    pBuffer);
#endif

#ifdef __cplusplus
}
#endif

#endif
