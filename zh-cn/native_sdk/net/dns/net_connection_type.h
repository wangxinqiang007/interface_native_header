/*
 * Copyright (c) 2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef NATIVE_NET_CONN_TYPE_H
#define NATIVE_NET_CONN_TYPE_H

/**
 * @addtogroup NetConnection
 * @{
 *
 * @brief 为网络管理数据网络连接模块提供C接口.
 *
 * @since 11
 * @version 1.0
 */

/**
 * @file net_connection_type.h
 * @brief 为网络管理数据网络连接模块提供C接口.
 *
 * @library libnet_connection.so
 * @syscap SystemCapability.Communication.NetManager.Core
 * @since 11
 * @version 1.0
 *
 */

#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif

#define NETCONN_MAX_NET_SIZE 32
#define NETCONN_MAX_BEARER_TYPE_SIZE 32
#define NETCONN_MAX_CAP_SIZE 32
#define NETCONN_MAX_ADDR_SIZE 32
#define NETCONN_MAX_ROUTE_SIZE 64
#define NETCONN_MAX_EXCLUSION_SIZE 256
#define NETCONN_MAX_STR_LEN 256

/**
 * @brief 网络能力集.
 *
 * @since 11
 * @version 1.0
 */
typedef enum NetConn_NetCap {
    /** MMS */
    NETCONN_NET_CAPABILITY_MMS = 0,
    /** 非计量网络 */
    NETCONN_NET_CAPABILITY_NOT_METERED = 11,
    /** Internet */
    NETCONN_NET_CAPABILITY_INTERNET = 12,
    /** 非VPN */
    NETCONN_NET_CAPABILITY_NOT_VPN = 15,
    /** 已验证 */
    NETCONN_NET_CAPABILITY_VALIDATED = 16,
} NetConn_NetCap;

/**
 * @brief 网络载体类型.
 *
 * @since 11
 * @version 1.0
 */
typedef enum NetConn_NetBearerType {
    /** 蜂窝网络 */
    NETCONN_BEARER_CELLULAR = 0,
    /** WIFI */
    NETCONN_BEARER_WIFI = 1,
    /** Ethernet */
    NETCONN_BEARER_ETHERNET = 3,
} NetConn_NetBearerType;

/**
 * @brief 存放网络ID.
 *
 * @since 11
 * @version 1.0
 */
typedef struct NetConn_NetHandle {
    /** 网络标识符 */
    int32_t netId;
} NetConn_NetHandle;

/**
 * @brief 网络能力集.
 *
 * @since 11
 * @version 1.0
 */
typedef struct NetConn_NetCapabilities {
    /** 上行带宽 */
    uint32_t linkUpBandwidthKbps;
    /** 下行带宽 */
    uint32_t linkDownBandwidthKbps;
    /** 网络能力列表 */
    NetConn_NetCap netCaps[NETCONN_MAX_CAP_SIZE];
    /** 网络能力列表的实际size */
    int32_t netCapsSize;
    /** 承载类型列表 */
    NetConn_NetBearerType bearerTypes[NETCONN_MAX_BEARER_TYPE_SIZE];
    /** 承载类型列表的实际size */
    int32_t bearerTypesSize;
} NetConn_NetCapabilities;

/**
 * @brief 网络地址.
 *
 * @since 11
 * @version 1.0
 */
typedef struct NetConn_NetAddr {
    /** 网络地址族 */
    uint8_t family;
    /** 前缀长度 */
    uint8_t prefixlen;
    /** 端口号 */
    uint8_t port;
    /** 地址 */
    char address[NETCONN_MAX_STR_LEN];
} NetConn_NetAddr;

/**
 * @brief 路由配置信息.
 *
 * @since 11
 * @version 1.0
 */
typedef struct NetConn_Route {
    /** 网络接口 */
    char iface[NETCONN_MAX_STR_LEN];
    /** 目标地址 */
    NetConn_NetAddr destination;
    /** 网关地址 */
    NetConn_NetAddr gateway;
    /** 是否存在网关 */
    int32_t hasGateway;
    /** 是否是默认路由 */
    int32_t isDefaultRoute;
} NetConn_Route;

/**
 * @brief 代理配置信息.
 *
 * @since 11
 * @version 1.0
 */
typedef struct NetConn_HttpProxy {
    /** 主机名 */
    char host[NETCONN_MAX_STR_LEN];
    /** 代理服务器的排除列表 */
    char exclusionList[NETCONN_MAX_EXCLUSION_SIZE][NETCONN_MAX_STR_LEN];
    /** 排除列表的实际大小 */
    int32_t exclusionListSize;
    /** 端口号 */
    uint16_t port;
} NetConn_HttpProxy;

/**
 * @brief 网络链接信息.
 *
 * @since 11
 * @version 1.0
 */
typedef struct NetConn_ConnectionProperties {
    /** 网络接口的名称 */
    char ifaceName[NETCONN_MAX_STR_LEN];
    /** 网络连接的域名信息 */
    char domain[NETCONN_MAX_STR_LEN];
    /** TCP 缓冲区大小 */
    char tcpBufferSizes[NETCONN_MAX_STR_LEN];
    /** MTU */
    uint16_t mtu;
    /** 地址列表 */
    NetConn_NetAddr netAddrList[NETCONN_MAX_ADDR_SIZE];
    /** 地址列表的实际size */
    int32_t netAddrListSize;
    /** DNS 列表 */
    NetConn_NetAddr dnsList[NETCONN_MAX_ADDR_SIZE];
    /** DNS 列表的实际size */
    int32_t dnsListSize;
    /** 路由列表 */
    NetConn_Route routeList[NETCONN_MAX_ROUTE_SIZE];
    /** 路由列表的实际大小 */
    int32_t routeListSize;
    /** HTTP 代理信息 */
    NetConn_HttpProxy httpProxy;
} NetConn_ConnectionProperties;

/**
 * @brief 网络列表.
 *
 * @since 11
 * @version 1.0
 */
typedef struct NetConn_NetHandleList {
    /** netHandle列表 */
    NetConn_NetHandle netHandles[NETCONN_MAX_NET_SIZE];
    /** netHandleList的实际大小 */
    int32_t netHandleListSize;
} NetConn_NetHandleList;

/**
 * @brief 指向自定义 DNS 解析器的指针.
 *
 * @param host 要查询的主机名.
 * @param serv 服务名称.
 * @param hint 指向addrinfo结构的指针.
 * @param res 存储DNS查询结果并以链表形式返回.
 *
 * @since 11
 * @version 1.0
 */
typedef int (*OH_NetConn_CustomDnsResolver)(const char *host, const char *serv,
    const struct addrinfo *hint, struct addrinfo **res);
#ifdef __cplusplus
}
#endif

/** @} */
#endif /* NATIVE_NET_CONN_TYPE_H */