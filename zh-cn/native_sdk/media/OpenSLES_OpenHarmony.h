/*
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @file OpenSLES_OpenHarmony.h
 *
 * @brief 提供音频文件的相关操作
 *
 * 功能包括音频播放文件和录音文件操作
 *
 * @since 9
 * @version 1.0
 */

#ifndef OPENSLES_OPENHARMONY_H
#define OPENSLES_OPENHARMONY_H

#include<OpenSLES.h>
#include<OpenSLES_Platform.h>

/*---------------------------------------------------------------------------*/
/* OH Buffer队列接口                                                         */
/*---------------------------------------------------------------------------*/

extern const SLInterfaceID SL_IID_OH_BUFFERQUEUE;

struct SLOHBufferQueueItf_;
typedef const struct SLOHBufferQueueItf_ * const * SLOHBufferQueueItf;

/**
 * @brief 回调函数
 *
 * 如果是播放操作，回调函数用来读取待播放的音频文件
 * 如果是录音操作，则从filledBufferQ_队列中获取携带录音数据的buffer
 *
 * @param self 表示调用该函数的BufferQueue接口对象
 * @param pContext 播放时表示播放音频文件，录音时表示录音存储文件
 * @param size 表示一次播放或录音时的buffer数据长度
 * @return
 * @since 9
 * @version 1.0
 */
typedef void (SLAPIENTRY *SlOHBufferQueueCallback) (
    SLOHBufferQueueItf caller,
    void *pContext,
    SLuint32 size
);

/** OH Buffer队列状态 **/
typedef struct SLOHBufferQueueState_ {
    SLuint32 count;
    SLuint32 index;
} SLOHBufferQueueState;

struct SLOHBufferQueueItf_ {
/**
 * @brief 根据情况将buffer加到相应队列中
 *
 * 如果是播放操作，则将带有音频数据的buffer插入到filledBufferQ_队列中
 * 如果是录音操作，则将录音使用后的空闲buffer插入到freeBufferQ_队列中
 *
 * @param self 表示调用该函数的BufferQueue接口对象
 * @param buffer 播放时表示带有音频数据的buffer，录音时表示已存储完录音数据后的空闲buffer
 * @param size 表示buffer的大小
 * @return 返回操作是否成功，出错时返回对应的错误编码
 * @since 9
 * @version 1.0
 */
SLresult (*Enqueue) (
    SLOHBufferQueueItf self,
    const void *buffer,
    SLuint32 size
);

/**
 * @brief 释放BufferQueue接口对象
 *
 * @param self 表示调用该函数的BufferQueue接口对象将被释放
 * @return 返回操作是否成功，出错时返回对应的错误编码
 * @since 9
 * @version 1.0
 */
SLresult (*Clear) (
    SLOHBufferQueueItf self
);

/**
 * @brief 获取BufferQueue接口对象状态
 *
 * @param self 表示调用该函数的BufferQueue接口对象
 * @param state BufferQueue的当前状态
 * @return 返回操作是否成功，出错时返回对应的错误编码
 * @since 9
 * @version 1.0
 */
SLresult (*GetState) (
    SLOHBufferQueueItf self,
    SLOHBufferQueueState *state
);

/**
 * @brief 根据情况获取相应的buffer
 *
 * 如果是播放操作，则从freeBufferQ_队列中获取空闲buffer
 * 如果是录音操作，则从filledBufferQ_队列中获取携带录音数据的buffer
 *
 * @param self 表示调用该函数的BufferQueue接口对象
 * @param buffer 播放时表示空闲的buffer，录音时表示携带录音数据的buffer
 * @param size 表示buffer的大小
 * @return 返回操作是否成功，出错时返回对应的错误编码
 * @since 9
 * @version 1.0
 */
SLresult (*GetBuffer) (
    SLOHBufferQueueItf self,
    SLuint8** buffer,
    SLuint32& size
);

/**
 * @brief 注册回调函数
 *
 * @param self 表示调用该函数的BufferQueue接口对象
 * @param callback 播放/录音时注册的回调函数
 * @param pContext 播放时传入待播放音频文件，录音时传入将要录制的音频文件
 * @return 返回操作是否成功，出错时返回对应的错误编码
 * @since 9
 * @version 1.0
 */
SLresult (*RegisterCallback) (
    SLOHBufferQueueItf self,
    SlOHBufferQueueCallback callback,
    void* pContext
);
};
#endif
