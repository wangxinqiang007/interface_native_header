/*
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @addtogroup Audio
 * @{
 *
 * @brief Audio模块接口定义
 *
 * 音频接口涉及自定义类型、驱动加载接口、驱动适配器接口、音频播放（render）接口、音频录音（capture）接口等
 *
 * @since 1.0
 * @version 1.0
 */

/**
 * @file audio_control.h
 *
 * @brief Audio控制的接口定义文件
 *
 * @since 1.0
 * @version 1.0
 */

#ifndef AUDIO_CONTROL_H
#define AUDIO_CONTROL_H

#include "audio_types.h"


/**
 * @brief AudioControl音频控制接口
 *
 * 提供音频播放（render）或录音（capture）需要的公共控制驱动能力，包括Start、Stop、Pause、Resume、Flush等
 *
 * @since 1.0
 * @version 1.0
 */
struct AudioControl {
    /**
     * @brief 启动一个音频播放（render）或录音（capture）处理
     *
     * @param handle 待操作的音频句柄
     * @return 成功返回值0，失败返回负值
     * @see Stop
     */
    int32_t (*Start)(AudioHandle handle);

    /**
     * @brief 停止一个音频播放（render）或录音（capture）处理
     *
     * @param handle 待操作的音频句柄
     * @return 成功返回值0，失败返回负值
     * @see Start
     */
    int32_t (*Stop)(AudioHandle handle);

    /**
     * @brief 暂停一个音频播放（render）或录音（capture）处理
     *
     * @param handle 待操作的音频句柄
     * @return 成功返回值0，失败返回负值
     * @see Resume
     */
    int32_t (*Pause)(AudioHandle handle);

    /**
     * @brief 恢复一个音频播放（render）或录音（capture）处理
     *
     * @param handle 待操作的音频句柄
     * @return 成功返回值0，失败返回负值
     * @see Pause
     */
    int32_t (*Resume)(AudioHandle handle);

    /**
     * @brief 刷新音频缓冲区buffer中的数据
     *
     * @param handle 待操作的音频句柄
     * @return 成功返回值0，失败返回负值
     */
    int32_t (*Flush)(AudioHandle handle);

    /**
     * @brief 设置或去设置设备的待机模式
     *
     * @param handle 待操作的音频句柄
     * @return 设置设备待机模式成功返回值0，再次执行后去设置待机模式成功返回正值，失败返回负值
     */
    int32_t (*TurnStandbyMode)(AudioHandle handle);

    /**
     * @brief Dump音频设备信息
     *
     * @param handle 待操作的音频句柄
     * @param range Dump信息范围，分为简要信息、全量信息
     * @param fd 指定Dump目标文件
     * @return 成功返回值0，失败返回负值
     */
    int32_t (*AudioDevDump)(AudioHandle handle, int32_t range, int32_t fd);
};

#endif /* AUDIO_CONTROL_H */
/** @} */
