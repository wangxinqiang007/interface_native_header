/*
 * Copyright (c) 2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

 /**
 * @addtogroup IntelligentVoiceTrigger
 * @{
 *
 * @brief IntelligentVoiceTrigger模块向上层服务提供了统一接口。
 *
 * 上层服务开发人员可根据IntelligentVoiceTrigger模块提供的向上统一接口获取如下能力：触发器适配器加载卸载、智能语音触发器模型加载卸载、底层唤醒业务启动停止等。
 *
 * @since 4.0
 * @version 1.0
 */

/**
 * @file IIntellVoiceTriggerManager.idl
 *
 * @brief IntelligentVoiceTrigger模块触发器管理接口，包括触发器适配器加载、触发器适配器卸载等。
 *
 * 模块包路径：ohos.hdi.intelligent_voice.trigger.v1_0
 *
 * 引用：
 * - ohos.hdi.intelligent_voice.trigger.v1_0.IntellVoiceTriggerTypes
 * - ohos.hdi.intelligent_voice.trigger.v1_0.IIntellVoiceTriggerAdapter
 *
 * @since 4.0
 * @version 1.0
 */

package ohos.hdi.intelligent_voice.trigger.v1_0;

import ohos.hdi.intelligent_voice.trigger.v1_0.IntellVoiceTriggerTypes;
import ohos.hdi.intelligent_voice.trigger.v1_0.IIntellVoiceTriggerAdapter;

 /**
 * @brief IntelligentVoiceTrigger模块向上层服务提供了智能语音触发器管理接口。
 *
 * 上层服务开发人员可根据IntelligentVoiceTrigger模块提供的向上智能语音触发器管理接口实现驱动适配器加载、驱动适配器卸载等功能。
 *
 * @since 4.0
 * @version 1.0
 */
interface IIntellVoiceTriggerManager {
    /**
     * @brief 加载一个触发器适配器。
     *
     * @param descriptor 智能语音触发器适配器描述符，信息包含适配器名称，具体参考{@link IntellVoiceTriggerAdapterDsecriptor}。
     * @param adapter 智能语音触发器适配器，具体参考{@link IIntellVoiceTriggerAdapter}。
     *
     * @return 如果操作成功，则返回0。
     * @return 如果操作失败，则返回负值。
     *
     * @since 4.0
     * @version 1.0
     */
    LoadAdapter([in] struct IntellVoiceTriggerAdapterDsecriptor descriptor, [out] IIntellVoiceTriggerAdapter adapter);
    /**
     * @brief 卸载一个触发器适配器。
     *
     * @param descriptor 智能语音触发器适配器描述符，信息包含适配器名称，具体参考{@link IntellVoiceTriggerAdapterDsecriptor}。
     *
     * @return 如果操作成功，则返回0。
     * @return 如果操作失败，则返回负值。
     *
     * @since 4.0
     * @version 1.0
     */
    UnloadAdapter([in] struct IntellVoiceTriggerAdapterDsecriptor descriptor);
}
/** @} */